<div class="panel-body">

    <div class="row">
        <!--  tab  content-->
        <div class="tab-content">
            <!-- General tab-->
            <div class="tab-pane active" id="tab-general">

                    <div class="col-lg-8 col-lg-offset-2">


                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <div class="form-group  {{ $errors->has('nb_jour_conges') ? 'has-error' : '' }}">
                        {!! Form::label('Nomber des jours du conges') !!}
                        <div class="controls">
                            {!! Form::text('nb_jouer_conge_demander', null, array('class' => 'form-control')) !!}
                            <span class="help-block">{{ $errors->first('nb_jour_conges', ':message') }}</span>
                        </div>
                    </div>

                        <div class="form-group  {{ $errors->has('date_de_debut_conge') ? 'has-error' : '' }}" id="groupDebutConge">
                            {!! Form::label('Date debut conges') !!}
                            <div class="controls">
                                {!! Form::text('date_de_debut_conge', null, array('class' => 'form-control','id'=>'dateDebutConge')) !!}
                                <span class="help-block">{{ $errors->first('date_de_debut_conge', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group  {{ $errors->has('date_de_fin_conge') ? 'has-error' : '' }}" id="groupFindConge">
                            {!! Form::label('Date fin conges') !!}
                            <div class="controls">
                                {!! Form::text('date_de_fin_conge', null, array('class' => 'form-control','id'=>'dateFinConge')) !!}
                                <span class="help-block">{{ $errors->first('date_de_fin_conge', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Type de congé?: ') !!}
                            <div class="row">
                                               <br>
                                <div class="col-lg-12">
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline filecheckcong ">
                                            {!! Form::checkbox('type_conge', 'maladie_obligation',  isset($conge)?$conge->type_conge=='maladie_obligation' : null, ['class' => 'filecheck']) !!}
                                            Maladie Obligation
                                           </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline filecheckcong ">
                                            {!! Form::checkbox('type_conge','conge',isset($conge) ?$conge->type_conge=='conge' : null, ['class' => 'filecheck']) !!}
                                            Congé
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline filecheckcong ">
                                            {!! Form::checkbox('type_conge', 'deces', isset($conge)?$conge->type_conge=='deces' : null, ['class' => 'filecheck']) !!}Décés

                                        </label>
                                        </div>
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline filecheckcong ">
                                            {!! Form::checkbox('type_conge', 'conge_sans_solde', isset($conge)?$conge->type_conge=='conge_sans_solde' : null, ['class' => 'filecheck']) !!}Congé sans solde

                                        </label>
                                        </div>
                                    </div>
                                        <br><br>
                                <div class="col-md-12">
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline filecheckcong ">
                                            {!! Form::checkbox('type_conge', 'militaires', isset($conge)?$conge->type_conge=='militaires' : null, ['class' => 'filecheck']) !!}militaires

                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline filecheckcong ">
                                            {!! Form::checkbox('type_conge', 'activites_judiciaires', isset($conge)?$conge->type_conge=='activites_judiciaires' : null, ['class' => 'filecheck']) !!}Activités
                                            judiciaires

                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline filecheckcong ">
                                            {!! Form::checkbox('type_conge', 'conge_parental', isset($conge)?$conge->type_conge=='conge_parental' : null, ['class' => 'filecheck']) !!}
                                            Congé parental

                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline filecheckcong">
                                            {!! Form::checkbox('type_conge', 'autre', isset($conge)?$conge->type_conge=='autre' : null, ['class' => 'filecheck']) !!}
                                           Autre

                                        </label>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group  {{ $errors->has('conge_description') ? 'has-error' : '' }}" id="groupConge_descritption">
                            {!! Form::label('Description') !!}
                            <div class="controls">
                                {!! Form::textarea('description', null, array('class' => 'form-control','id'=>'conge_description','rows'=>"3")) !!}
                                <span class="help-block">{{ $errors->first('conge_description', ':message') }}</span>
                            </div>
                        </div>

                    </div>  <!-- /.col-lg-6-->
            </div>     <!-- tab pane -->
        </div> <!--tab content -->

        <div class="col-lg-12">
            <div class="col-lg-3 col-lg-push-3">
                <button type="reset" class="btn btn-danger btn-block">Anuuler</button>
                </button>
            </div>
            <div class="col-lg-3  col-lg-push-3">
                            {!! Form::submit($submitButtonText,['class'=>
                            'btn btn btn-success btn-block']) !!}



            </div>
        </div>

    </div><!-- /div row -->

</div><!-- /panel-body -->