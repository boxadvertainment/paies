<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatePersonnelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post'=>'required|min:3',
            'nom'=>'required|min:3',
            'prenom'=>'required|min:3',
            'cin'=>'required|min:3',
            'non_banque'=>'required|min:3',
            'non_banque'=>'required|min:3',
            'tele_personnel'=>'required|numeric|min:8',
            'adresse_email'=>'required|email',
            'etat_sante'=>'required',
            'date_naissance'=> 'required',
            'date_entree'=>'required',
            'num_cnss'=>'required',
            'rib_banque'=>'required',
            'lieu_naissance'=>'required',
            'fumez_vous'=>'required',
            'buvze_vous'=>'required',
            //
        ];

    }
}
